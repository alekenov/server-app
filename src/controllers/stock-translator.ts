import { Transliterate } from '../utils/transliterate';
import { translate } from './translate';
import async from 'async';
import { CallbackInterface } from '../utils/callback-interface';
import { Pool, Client } from 'pg';

export class StockTranslator
{
  constructor()
  {
    this.init();
  }

  init(): void
  {
    const client = new Client({
      user    : 'postgres',
      host    : 'localhost',
      database: 'stock',
      password: 'aOsQRYw8agiD',
      port    : 5432
    });
    client.connect();

    client.query('select * from base.filter_item_translate where url isnull or url = \'\'')
      .then((res) =>
      {
        async.each(res.rows, (translate: any, callback: CallbackInterface) =>
        {
          const query = {
            text: 'update base.filter_item_translate set url = $1 where id = $2',
            values: [Transliterate.transliterate(translate.value), translate.id],
          };

          client.query(query).then((res) =>
            {
              console.log('success', res);
              callback();
            });
          // this.createEngTranslate(translate).then(() =>
          // {
          //   callback();
          // });
        }, (err) =>
        {
        });

        client.end();
      })
      .catch(e =>
      {
        console.log('e', e);
        client.end();
      });
  }

  createEngTranslate(translate: any): Promise<any>
  {
    return new Promise((resolve, reject) =>
    {
      const eng: any = {
        id         : null,
        language_id: 2,
        value      : null,
        filter_id  : translate.filter_id,
        url        : null
      };

      Promise.all([
        this.translate(translate.value),
      ])
        .then((value) =>
        {
          eng.value = value[0];
          eng.url   = Transliterate.transliterate(value[0]);

          this.saveTranslates([eng]);

          resolve();
        });
    });
  }

  /**
   * Save translates for bouquet
   *
   * @param translates
   */
  saveTranslates(translates: any[]): void
  {
    const client = new Client({
      user    : 'postgres',
      host    : 'localhost',
      database: 'stock',
      password: 'aOsQRYw8agiD',
      port    : 5432
    });
    client.connect();

    async.each(translates, (translate: any, callback: CallbackInterface) =>
    {
      const query = {
        text: 'INSERT INTO base.filter_translate(value, filter_id, language_id, url) VALUES($1, $2, $3, $4)',
        values: [translate.value, translate.filter_id, translate.language_id, translate.url],
      };

      client.query(query)
        .then((res) =>
        {
          console.log('success', res);
          callback();
        })
        .catch(e =>
        {
          console.log('e', e);
          callback();
        });
    }, (err) =>
    {
      if (err)
      {
        console.log('Order moved to abandoned unsuccessfully');
      }
      client.end();
    });
  }

  /**
   * Translate
   *
   * @param text
   */
  translate(text: string): Promise<string>
  {
    text = text || '';
    return new Promise((resolve, reject) =>
    {
      if (text.length === 0)
      {
        resolve('');
        return;
      }

      translate(text, {from: 'ru', to: 'en'}).then((result: any) =>
      {
        resolve(result['text']);

      }).catch((err: any) =>
      {
        resolve('');
        console.error(err);
      });
    });
  }
}

const x = new StockTranslator();
