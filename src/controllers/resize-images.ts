import sharp from 'sharp';
import { Response, Request } from 'express';
import url from 'url';
import stream from 'stream';
import request from 'request';
import { join } from 'path';
import { AWS_path } from '../config/AWS';
import { LOGGER } from '../config/logger';
import fs from 'fs';

/**
 * Get image
 */
export let ResizeImages = (req: Request, res: Response, next: any) =>
{
  if (req.url.startsWith('/cvety/'))
  {
    return next(); // getStaticImage(req, res);
  }

  const reqUrl = url.parse(req.url, true);
  let action   = reqUrl.pathname;

  // Does this request contain an image action?
  const regex  = /\/(\d{1,3})x(\d{1,3})\//g;
  const result = regex.exec(action);

  if (!result)
  {
    return return404(res, 'no dimensions found in request', 240, 240);
  }
  const actionDir   = result[0];
  const widthValue  = parseInt(result[1], 10);
  const heightValue = parseInt(result[2], 10);

  // Reset the request directory
  action        = action.replace(actionDir, '');
  const reqFile = AWS_path + action;

  // Get the extension
  const extension = reqFile.split('.').pop();

  // Is this an image request?
  if (!hasAllowedExtension(reqFile, ['.png', '.jpg', '.JPG', '.jpeg', '.JPEG']))
  {
    return404(res, 'request did not have allowed extension', widthValue, heightValue);
    return;
  }

  const transform = sharp()
    .resize(widthValue, heightValue)
    .toFormat('jpeg')
    .crop(sharp.strategy.entropy);

  const readStream = request(reqFile);

  // When the stream is done being read, end the response
  readStream.on('close', () =>
  {
    return return404(res, 'image file was not found: ' + reqFile, widthValue, heightValue);
  });

  readStream.on('response', (response: any) =>
  {
    if (response.statusCode === 404)
    {
      return return404(res, 'image file was not found: ' + reqFile, widthValue, heightValue);
    }
    res.statusCode = 200;
    // console.log(response.headers);
    res.setHeader('ETag', response.headers['etag']);
    res.setHeader('Last-Modified', response.headers['last-modified']);
    res.setHeader('Cache-Control', 'public, max-age=31556952000');
    res.setHeader('Expires', new Date(Date.now() + 31556952000).toUTCString());
    readStream.pipe(new stream.PassThrough()).pipe(transform).pipe(res);
  });
};

/**
 * Image not exist
 */
function return404(res: Response, e: string, widthValue: number, heightValue: number): void
{
  LOGGER.warn('resize 404: ', e);

  const emptyImagePath = join('dist', 'public', 'images', 'big.png');

  sharp(emptyImagePath)
    .resize(widthValue, heightValue)
    .crop(sharp.strategy.entropy)
    .toBuffer(function (err: Error, outputBuffer: any)
    {
      if (err)
      {
        throw err;
      }
      res.statusCode = 200;
      res.setHeader('Cache-Control', 'public, max-age=31556952000');
      res.setHeader('Expires', new Date(Date.now() + 31556952000).toUTCString());
      res.setHeader('Content-Type', 'image/png');
      res.end(outputBuffer, 'binary');
    });
}

/**
 * Check allowed image extensions
 */
function hasAllowedExtension(file: string, exts: string[]): boolean
{
  file = file.split('?')[0];
  return (new RegExp('(' + exts.join('|').replace(/\./g, '\\.') + ')$')).test(file);
}

export let ResizeImageStatic = (req: Request, res: Response) =>
{
  const fileName       = req.url.split('/').pop();
  const widthValue     = parseInt(req.query.width, 10);
  const heightValue    = parseInt(req.query.height, 10);
  const emptyImagePath = join('dist', 'public', 'images', 'big.png');

  // Is this an image request?
  if (!hasAllowedExtension(fileName, ['.png', '.jpg', '.JPG', '.jpeg', '.JPEG']))
  {
    return404(res, 'request did not have allowed extension', widthValue, heightValue);
    return;
  }

  let filePath = join('dist', 'public', 'images', 'bouquets', fileName); // join('uploads', reqFile);

  if (isNaN(widthValue) || isNaN(heightValue))
  {
    if (!fs.existsSync(filePath))
    {
      filePath = emptyImagePath;
    }
    sharp(filePath)
      .toFormat('jpeg')
      .toBuffer(function (err: Error, outputBuffer: any)
      {
        if (err)
        {
          return404(res, 'not valid image', widthValue, heightValue);
          return;
        }
        res.statusCode = 200;
        res.setHeader('Cache-Control', 'public, max-age=31556952000');
        res.setHeader('Expires', new Date(Date.now() + 31556952000).toUTCString());
        res.setHeader('Content-Type', 'image/jpeg');
        res.end(outputBuffer, 'binary');
      });
  }
  else
  {
    filePath = filePath.split('?')[0];

    if (!fs.existsSync(filePath))
    {
      filePath = emptyImagePath;
    }

    sharp(filePath)
      .resize(widthValue, heightValue)
      .crop(sharp.strategy.entropy)
      .toFormat('jpeg')
      .toBuffer(function (err: Error, outputBuffer: any)
      {
        if (err)
        {
          return404(res, 'not valid image', widthValue, heightValue);
          return;
        }
        res.statusCode = 200;
        res.setHeader('Cache-Control', 'public, max-age=31556952000');
        res.setHeader('Expires', new Date(Date.now() + 31556952000).toUTCString());
        res.setHeader('Content-Type', 'image/jpeg');
        res.end(outputBuffer, 'binary');
      });
  }
};