import { Pg } from '../config/pg-client-settings';
import { SQL } from '../utils/sql';
import { Client } from 'pg';
import pg from 'pg';
import { LOGGER } from '../config/logger';
import async from 'async';
import request from 'request';
import { CallbackInterface } from '../utils/callback-interface';
import querystring from 'querystring';
import { parseString } from 'xml2js';
import * as https from 'https';

export class LekenPaymentCheck
{
  protected orders: {order_id: number, id: number}[];

  constructor()
  {
  }

  /**
   * Init class methods
   */
  run(): void
  {
    const client: Client        = new Pg().clientMarket;
    // const query: pg.QueryConfig = SQL`SELECT order_id, id FROM base.deals WHERE pay = 'kazkom' AND is_payment_success <> true limit 1`;
    const query: pg.QueryConfig = SQL`SELECT order_id, id
     FROM base.deals
     WHERE created_at > now() :: date - 2 AND pay = 'kazkom' and (payment_checked isnull or payment_checked = false)`;

    client.query(query)
      .then(res =>
      {
        if (res.rows.length > 0)
        {
          this.orders = res.rows;
          this.eachOrders();
        }

        client.end();
      })
      .catch(e =>
      {
        LOGGER.error(e.stack);

        client.end();
      });
  }

  /**
   * Start process for each order
   * @returns {any}
   */
  private eachOrders(): any
  {
    async.each(this.orders, (order: {order_id: number, id: number}, callback: CallbackInterface) =>
    {
      this.init(order.order_id)
        .then(() => callback())
        .catch(() => callback());
    }, (err) =>
    {
      if (err)
      {
        LOGGER.error('Order moved to abandoned unsuccessfully');
      }
    });
  }

  runTest(): void
  {
    this.init(100343);
  }

  /**
   * Start request
   *
   * @param {number} id
   * @returns {Promise<any>}
   */
  init(id: number): Promise<any>
  {
    return new Promise((resolve, reject) =>
    {
      const postData = {
        order_id: checkOrderNumber(id)
      };
      const postBody = querystring.stringify(postData);

      const options = {
        host   : 'pay.leken.kz',
        path   : '/kazkom-leken/init.php',
        method : 'POST',
        headers: {
          'Content-Type'  : 'application/x-www-form-urlencoded',
          'Content-Length': postBody.length
        }
      };

      const postreq = https.request(options, (res: any) =>
      {
        res.setEncoding('utf8');
        res.on('data', (chunk: string) =>
        {
          this.sendToKazkom(chunk, id)
            .then(() => resolve())
            .catch((err: any) => reject(err));
        });
      });
      postreq.write(postBody);
      postreq.end();
    });
  }

  /**
   * Send request to kazkom
   *
   * @param {string} patch
   * @param {number} orderId
   * @returns {Promise<any>}
   */
  private sendToKazkom(patch: string, orderId: number): Promise<any>
  {
    return new Promise((resolve, reject) =>
    {
      request('https://epay.kkb.kz/jsp/remote/checkOrdern.jsp?' + patch, (error, response, body) =>
      {
        parseString(body, (err, result) =>
        {
          if (result && result.document)
          {
            const paymentInfo = result.document.bank[0]['response'][0]['$'];
            this.savePaymentInfo(paymentInfo, orderId);
            return resolve();
          }

          reject();
        });
      });
    });
  }

  /**
   * Save payment info
   *
   * @param data
   * @param {number} orderId
   */
  private savePaymentInfo(data: any, orderId: number): void
  {
    let query;

    if (data.payment === 'true')
    {
      query = SQL`UPDATE base.deals
                  SET payment_checked = true,
                  status_id = 4,
                  is_payment_success = true WHERE order_id = ${orderId}`;
    }
    else
    {
      query = SQL`UPDATE base.deals SET is_payment_success = null, payment_checked = null WHERE order_id = ${orderId}`;
    }

    const client: Client = new Pg().clientMarket;
    client.query(query)
      .then((res) =>
      {
        console.log('success', res);
        client.end();
      })
      .catch(e =>
      {
        console.log('e', e);
        LOGGER.error(e.stack);

        client.end();
      });
  }
}

/**
 * Check order number
 *
 * @param {number} number
 * @returns {string}
 */
function checkOrderNumber(number: number): string
{
  const numberStr = number.toString();
  if (numberStr.length == 4)
  {
    return '00' + numberStr;
  }
  else if (numberStr.length == 5)
  {
    return '0' + numberStr;
  }
  return numberStr;
}