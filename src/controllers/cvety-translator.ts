import { PgListener } from '../apps/stock-db-notify-server';
import { translate } from './translate';
import { Client } from 'pg';
import { Transliterate } from '../utils/transliterate';
import { SQL } from '../utils/sql';

export class CvetyTranslator
{
  constructor()
  {
    this.watchChanges();
  }

  watchChanges(): void
  {
    PgListener.on('pg-listen', (value: any) =>
    {
      if (value.table === 'bouquet_translate')
      {
        const bouquetTranslate = value.row;

        console.log('bouquetTranslate', bouquetTranslate.name);

        if (bouquetTranslate.language_id === 1)
        {
          this.getTranslates(bouquetTranslate);
        }
      }
    });
  }

  getTranslates(bouquetTranslate: any): void
  {
    const translate: any = {
      id           : null,
      language_id  : 2,
      name         : null,
      description  : null,
      bouquet_id   : bouquetTranslate.bouquet_id,
      url          : null,
      bouquet_price: bouquetTranslate.bouquet_price
    };

    const client = new Client({
      user    : 'postgres',
      host    : 'localhost',
      database: 'stock',
      password: 'aOsQRYw8agiD',
      port    : 5432
    });
    client.connect();

    const promises = [this.translate(bouquetTranslate.name)];

    if (bouquetTranslate.description)
    {
      promises.push(this.translate(bouquetTranslate.description));
    }

    const query = SQL`select * from base.bouquet_translate where bouquet_id = ${bouquetTranslate.bouquet_id} and language_id = 2`;

    client.query(query)
      .then((res) =>
      {
        if (res.rows.length > 0)
        {
          translate.id = res.rows[0].id;
        }

        Promise.all(promises).then((value: any[]) =>
        {
          translate.name = value[0];
          translate.url  = Transliterate.transliterate(value[0]);

          if (!!value[1])
          {
            translate.description = value[1];
          }

          this.saveTranslate(translate);
        });

        client.end();
      })
      .catch(e =>
      {
        console.log('e', e);
        client.end();
      });
  }

  saveTranslate(bouquetTr: any): void
  {
    const client = new Client({
      user    : 'postgres',
      host    : 'localhost',
      database: 'stock',
      password: 'aOsQRYw8agiD',
      port    : 5432
    });
    client.connect();

    let query;

    if (!!bouquetTr.id)
    {
      query = SQL`UPDATE base.bouquet_translate
                        SET url = ${bouquetTr.url},
                        name = ${bouquetTr.name},
                        description = ${bouquetTr.description}
                        WHERE id = ${bouquetTr.id}`;
    }
    else
    {
      query = SQL`INSERT INTO base.bouquet_translate
                        (url, name, description, bouquet_id)
                         VALUES (${bouquetTr.url}, ${bouquetTr.name}, ${bouquetTr.description}, ${bouquetTr.bouquet_id})`;
    }

    client.query(query)
      .then((res) =>
      {
        console.log('success', res);
        client.end();
      })
      .catch(e =>
      {
        console.log('e', e);
        client.end();
      });
  }

  /**
   * Translate
   *
   * @param text
   */
  translate(text: string): Promise<string>
  {
    text = text || '';
    return new Promise((resolve, reject) =>
    {
      if (text.length === 0)
      {
        resolve('');
        return;
      }

      translate(text, {from: 'ru', to: 'en'}).then((result: any) =>
      {
        resolve(result['text']);

      }).catch((err: any) =>
      {
        resolve('');
        console.error(err);
      });
    });
  }
}