import request from 'request';
import { LOGGER } from '../config/logger';
import { DbLogger } from './db-logger';

const USERNAME       = 'love@cvety.kz';
const PASSWORD       = 'HGCh47d';
const ADD_ORDERS_URL = 'https://esputnik.com/api/v1/orders';

export class Esputnik
{
  body: any;

  constructor()
  {
  }

  run(data: any)
  {
    this.body = {
      'orders': [{
        'externalOrderId'   : data.row.orderid,
        'externalCustomerId': data.row.sender_email,
        'totalCost'         : data.row.amount,
        'status'            : 'DELIVERED',
        'date'              : data.row.delivery_date,
        'email'             : data.row.sender_email,
        'phone'             : data.row.sender_phone,
        'firstName'         : data.row.sender_name,
        'lastName'          : '',
        'discount'          : 0,
        'deliveryAddress'   : data.row.delivery_address,
        /*"items":
            [
                {
                    "name": (data.row.order_list || '').replace(/<\/?[^>]+(>|$)/g, ""),
                    "quantity": 1,
                    "cost": data.row.budget_amount_value,
                }]*/
      }]
    };

    this.sendRequest();
  }

  /**
   * Send request
   */
  private sendRequest()
  {
    const auth       = 'Basic ' + new Buffer(USERNAME + ':' + PASSWORD).toString('base64');
    const headersOpt = {
      'Content-Type' : 'application/json',
      'Accept'       : 'application/json',
      'authorization': auth
    };

    const options = {
      uri    : ADD_ORDERS_URL,
      method : 'POST',
      json   : this.body,
      headers: headersOpt
    };

    request(options, function (error, response, body)
    {
      const logger = new DbLogger();

      if (error)
      {
        logger.log('esputnik error ' + error, 'error');
      }
      else
      {
        logger.log(response);
      }
    });
  }
}
