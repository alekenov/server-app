import { Client } from 'pg';
import { SQL } from '../utils/sql';
import pg from 'pg';
import async from 'async';
import nodemailer from 'nodemailer';
import handlebars from 'handlebars';
import { join } from 'path';
import { readFileSync } from 'fs';
import { Order } from '../models/order';
import { Pg } from '../config/pg-client-settings';
import { LOGGER } from '../config/logger';
import { MailSmtpSettings } from '../config/mail-smtp-settings';
import { CallbackInterface } from '../utils/callback-interface';

export class NotifyCustomerNotPayed
{
  protected orders: Order[];

  constructor()
  {
  }

  /**
   * Init class methods
   */
  run(): void
  {
    const client: Client      = new Pg().client;
    const selectNotPayedQuery = SQL`SELECT * FROM base.notified_orders`;

    client.query(selectNotPayedQuery)
      .then(res =>
      {
        if (res.rows.length > 0)
        {
          this.orders = [...res.rows].map(item => new Order(item));
          this.eachOrders();
        }
        client.end();
      })
      .catch(e =>
      {
        LOGGER.error(e.stack);

        client.end();
      });
  }

  /**
   * Start process for each order
   * @returns {any}
   */
  private eachOrders(): any
  {
    async.each(this.orders, (order: Order, callback: CallbackInterface) =>
    {
      this.sendEmail(order)
        .then(() =>
        {
          this.markNotify(order, callback);
        })
        .catch(error =>
        {
          LOGGER.error(`Notify not payed, email: ${order.senderEmail}, ` + error.message);
          callback(error);
        });

    }, (err) =>
    {
      if (err)
      {
        LOGGER.error('A mail failed to send');
      }
      else
      {
        LOGGER.info('All mails have been sends successfully');
      }
    });
  }

  /**
   * Send email to customer
   * @param {Order} order
   * @returns {Promise<any>}
   */
  private sendEmail(order: Order): Promise<any>
  {
    return new Promise((resolve, reject) =>
    {
      const mailPath    = join('dist', 'public', 'mail-templates', 'order-not-payed.html');
      const html        = readFileSync(mailPath).toString();
      const template    = handlebars.compile(html);
      const htmlToSend  = template(order);
      const transporter = nodemailer.createTransport(new MailSmtpSettings().data);

      const mailOptions = {
        to     : order.senderEmail,
        from   : '"admin" <love@cvety.kz>',
        subject: 'Cvety.kz заказ ' + order.orderid,
        html   : htmlToSend
      };
      transporter.sendMail(mailOptions, (error) =>
      {
        if (error)
        {
          reject(error);
        }
        else
        {
          resolve();
        }
      });
    });
  }

  /**
   * Mark order as notify
   * @param {Order} order
   * @param {CallbackInterface} callback
   */
  private markNotify(order: Order, callback: CallbackInterface): void
  {
    order.isNotify = true;

    const client: Client        = new Pg().client;
    const query: pg.QueryConfig = SQL`UPDATE base."order" SET is_notify = ${order.isNotify} WHERE id = ${order.id}`;

    client.query(query)
      .then(() =>
      {
        client.end();
        callback();
      })
      .catch(err =>
      {
        client.end();
        LOGGER.error(err);
        callback(err);
      });
  }
}
