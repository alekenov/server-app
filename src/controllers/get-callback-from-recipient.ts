import { Client } from 'pg';
import { Pg } from '../config/pg-client-settings';
import { Mobizon } from './mobizon';
import { DbLogger } from './db-logger';

export class GetCallbackFromRecipient
{
  constructor()
  {
  }

  getOrders(): void
  {
    const client = new Client(Pg.stockDbConfigObject());
    client.connect();

    const query = `select * from base.order_delivery_time
                   where (datetime > now() at time zone 'UTC' - interval '60 minutes')
                     and (datetime < now() at time zone 'UTC' - interval '59 minutes')`;

    client.query(query).then(res =>
    {
      if (res.rows.length > 0)
      {
        res.rows.forEach(row => this.mobizonSend(row));
      }

      client.end();
    }).catch(e =>
    {
      console.log(e);

      client.end();
    });
  }

  mobizonSend(row: any): void
  {
    if (!row.sender_phone)
    {
      return;
    }

    const logger = new DbLogger();
    const client = new Mobizon({
      recipient: row.sender_phone,
      from     : 'сvety'
    });

    client.text = `Оставьте отзыв к заказу - https://onetwoflowers.ru/rf/${row.random_uuid}`;
    client.sendMessage();
    logger.log(client.text);
  }
}
