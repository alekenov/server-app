import { Client } from 'pg';
import { Pg } from '../config/pg-client-settings';
import { LOGGER } from '../config/logger';
import moment from 'moment';
import request from 'request';
import { SQL } from '../utils/sql';
import pg from 'pg';

export class SendOrderToCvetyKz
{
  private orderUrl         = 'https://cvety.kz/crm_import/order.php';
  private statusHistoryUrl = 'https://cvety.kz/crm_import/status.php';

  constructor()
  {
  }

  /**
   * Init class methods
   */
  run(data: any, type: string): void
  {
    switch (type)
    {
      case 'new_order':
        this.newOrder(data);
        break;

      case 'save_status':
        this.statusHistory(data);
        break;
    }
  }

  /**
   * Notify site(cvety.kz) of new order
   * @param data
   */
  private newOrder(data: any): void
  {
    const id         = data.row.id;
    let deliveryDate = data.row.delivery_date;
    let deliveryTime = undefined;

    if (deliveryDate && deliveryDate.length > 0)
    {
      const fullDate = deliveryDate.split('T');
      if (fullDate.length > 0)
      {
        deliveryDate = fullDate[0];
        deliveryTime = fullDate[1];

        if (deliveryTime.length > 5)
        {
          deliveryTime = deliveryTime.substring(0, 5);
        }
      }
    }

    const bodyOpt = {
      'receiver_name'    : data.row.receiver_name || undefined,
      'recipient_phone'  : data.row.receiver_phone || undefined,
      'recipient_address': data.row.delivery_address || undefined,
      'sender_phone'     : data.row.sender_phone || undefined,
      'sender_email'     : data.row.sender_email || undefined,
      'delivery_date'    : deliveryDate || undefined,
      'delivery_time'    : deliveryTime || undefined,
      'postcard_text'    : data.row.postcard_text || undefined, // пожелание
      'pay'              : data.row.pay || undefined,
      'product'          : data.row.order_list || undefined
    };

    request.post({
      url : this.orderUrl,
      form: bodyOpt
    }, (error, response, newOrderId) =>
    {
      if (error)
      {
        return LOGGER.error('Notify site(cvety.kz) of new order ', error);
      }

      LOGGER.info('Notify site(cvety.kz) of new order', new Date().toJSON(), ', Заказ #', newOrderId, ', bodyOpt:', JSON.stringify(bodyOpt));

      if (newOrderId)
      {
        this.updateOrderId(id, newOrderId, data);
      }
    });
  }

  /**
   * Update orderid in db
   */
  private updateOrderId(id: number, newOrderId: string, data: any): void
  {
    const client: Client        = new Pg().client;
    const query: pg.QueryConfig = SQL`UPDATE base.order SET orderid = ${newOrderId} WHERE id = ${id}`;

    client.query(query)
      .then(() =>
      {
        // Status change
        const cloneData       = Object.assign({}, data);
        cloneData.row.orderid = newOrderId;

        this.statusHistory(cloneData);

        client.end();
      })
      .catch(err =>
      {
        LOGGER.error(err);

        client.end();
      });
  }

  /**
   * Notify site(cvety.kz) of status change
   * @param data
   */
  private statusHistory(data: any): void
  {
    const bodyOpt = {
      'id_site_order'     : data.row.orderid,
      'date_status_change': moment().format(),
      'status_id'         : data.row.status_id,
      'sender_name'       : data.row.sender_name,
      'link_to_photo'     : data.row.filepatch
    };

    request.post({
      url : this.statusHistoryUrl,
      form: bodyOpt
    }, (error, response, body) =>
    {
      if (error)
      {
        LOGGER.error('Notify site(cvety.kz) of status change', 'Заказ # ', bodyOpt['id_site_order'], ' ', error);
      }
      else
      {
        LOGGER.info('Notify site(cvety.kz) of status change', 'Заказ # ', bodyOpt['id_site_order'], ' ', new Date().toJSON(), ', body:', body, ', bodyOpt:', JSON.stringify(bodyOpt));
      }
    });
  }
}