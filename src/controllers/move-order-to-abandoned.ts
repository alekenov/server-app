import pg from 'pg';
import { Client } from 'pg';
import { SQL } from '../utils/sql';
import async from 'async';
import { Order } from '../models/order';
import { Pg } from '../config/pg-client-settings';
import { LOGGER } from '../config/logger';
import { CallbackInterface } from '../utils/callback-interface';

export class MoveOrderToAbandoned
{
  protected orders: Order[];

  constructor()
  {
  }

  /**
   * Init class methods
   */
  run(): void
  {
    const client: Client        = new Pg().client;
    const query: pg.QueryConfig = SQL`SELECT * FROM base.abandoned_orders`;

    client.query(query)
      .then(res =>
      {
        if (res.rows.length > 0)
        {
          this.orders = [...res.rows].map(item => new Order(item));
          this.eachOrders();
        }

        client.end();
      })
      .catch(e =>
      {
        LOGGER.error(e.stack);

        client.end();
      });
  }

  /**
   * Start process for each order
   * @returns {any}
   */
  private eachOrders(): any
  {
    async.each(this.orders, (order: Order, callback: CallbackInterface) =>
    {
      this.updateOrder(order, callback);

    }, (err) =>
    {
      if (err)
      {
        LOGGER.error('Order moved to abandoned unsuccessfully');
      }
      else
      {
        LOGGER.info('Order moved to abandoned successfully');
      }
    });
  }

  /**
   * Mark order as abandoned
   * @param {Order} order
   * @param {CallbackInterface} callback
   */
  private updateOrder(order: Order, callback: CallbackInterface): void
  {
    order.statusId = 10;

    const client: Client        = new Pg().client;
    const query: pg.QueryConfig = SQL`UPDATE base."order" SET status_id = ${order.statusId} WHERE id = ${order.id}`;

    client.query(query)
      .then(() =>
      {
        client.end();
        callback();
      })
      .catch(err =>
      {
        client.end();
        LOGGER.error(err);
        callback(err);
      });
  }
}
