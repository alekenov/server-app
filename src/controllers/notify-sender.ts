import { PgListener } from '../apps/stock-db-notify-server';
import { join } from 'path';
import { readFileSync } from 'fs';
import handlebars from 'handlebars';
import nodemailer from 'nodemailer';
import { MailSmtpSettings } from '../config/mail-smtp-settings';
import { DbLogger } from './db-logger';

export class NotifySender
{
  constructor()
  {
    this.watchChanges();
  }

  watchChanges(): void
  {
    PgListener.on('pg-listen', (value: any) =>
    {
      if (value.table === 'callback')
      {
        const callback = value.row;

        if (!!callback.sender_email && !!callback.recipient_photo)
        {
          this.sendMail(callback);
        }
      }
    });
  }

  sendMail(callback: any): void
  {
    const mailPath    = join('dist', 'public', 'mail-templates', 'sender-notify.html');
    const html        = readFileSync(mailPath).toString();
    const template    = handlebars.compile(html);
    const htmlToSend  = template({image: callback.recipient_photo});
    const transporter = nodemailer.createTransport(new MailSmtpSettings().data);

    const logger = new DbLogger();

    const mailOptions = {
      to     : callback.sender_email,
      from   : '"admin" <love@cvety.kz>',
      subject: 'Заказ успешно доставлен',
      html   : htmlToSend
    };
    transporter.sendMail(mailOptions, (error) =>
    {
      if (error)
      {
        logger.log(error, 'error');
        return;
      }
      else
      {
        logger.log('Send email to ' + callback.sender_email + ' success');
      }
    });
  }
}