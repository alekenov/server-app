import { Request, Response, NextFunction } from 'express';
import { Client } from 'pg';
import { Pg } from '../../config/pg-client-settings';
import request from 'request';
import { SQL } from '../../utils/sql';
import { Mobizon } from '../mobizon';
import { LOGGER } from '../../config/logger';

const CLOUD_PAYMENTS_API = 'https://api.cloudpayments.kz/payments/cards/charge';
const CLOUD_PAYMENTS_PUBLIC_ID = 'pk_4315af52b8084f0c2f5d7b40b0533';
const CLOUD_PAYMENTS_PASS_API = '3b8c1cdb1a6aad306a1aa231e3bd22db';
// const CLOUD_PAYMENTS_BASIC_AUTH_KEY = Buffer.from(`${CLOUD_PAYMENTS_PUBLIC_ID}:${CLOUD_PAYMENTS_PASS_API}`).toString('base64');


/**
 * Charge card proxy
 *
 * @param req
 * @param res
 */
export function chargeCard(req: Request, res: Response, next: NextFunction)
{
    req.pipe(request.post(CLOUD_PAYMENTS_API).auth(CLOUD_PAYMENTS_PUBLIC_ID, CLOUD_PAYMENTS_PASS_API)).pipe(res);
}

/**
 * Generate and send code by sms
 *
 * @param req
 * @param res
 */
export async function sendCode(req: Request, res: Response, next: NextFunction)
{
    const code = Math.floor(Math.random() * 10000) + 9999;
    const phone = req.params.phone;

    const client = new Client(Pg.marketplaceDbConfigObject());
    client.connect();

    const getPhone = SQL`SELECT phone_mob FROM base."user" WHERE phone_mob IS NOT NULL AND phone_mob = ${phone}`;

    client.query(getPhone)
        .then(async resBD => {
            if (resBD.rows.length > 0) {
                console.log('start send SMS');
                const query = SQL`INSERT INTO base.sms_verification(phone,code) VALUES (${phone}, ${code})
  ON CONFLICT(phone) DO UPDATE SET code = ${code}`;

                try {
                    const result = await client.query(query);
                    // console.log('success', result);

                    const moby = new Mobizon({
                        recipient: phone,
                        from: 'leken',
                        text: `${code} vash proverochnyi kod`
                    });

                    return moby.sendMessage().pipe(res);
                } catch (e) {
                    res.status(500).send(e);
                    console.log('e', e);
                } finally {
                    client.end();
                }
            } else {
                res.status(400).send();
            }

            client.end();
        })
        .catch(e =>
        {
            LOGGER.error(e.stack);
            res.status(500).send();
            client.end();
        });

}


export async function registerShop(req: Request, res: Response, next: NextFunction)
{
    // console.log(req.body);
    const body = req.body;
    const client = new Client(Pg.marketplaceDbConfigObject());
    client.connect();

    const queryOrganization = SQL`INSERT INTO base.organization (name, phone, city_codes, address, address_translate, schedule, enabled)
  VALUES (${body.shop}, ${body.phoneMob}, ${body.cityCodes}, ${body.address}, ${body.addressTranslate}, ${body.schedule}, TRUE) RETURNING id`;

    let organization_id = null;

    try {
        const result = await client.query(queryOrganization);
        organization_id = result.rows[0].id;
    } catch (e) {
        res.status(500).send(e);
        console.log('e', e);
    }

    let person_id = null;

    const queryAccount = SQL`INSERT INTO base.user (first_name, phone_mob, email, organization_id)
  VALUES
    (${body.shop}, ${body.phoneMob}, ${body.email}, ${organization_id}) RETURNING id`;

    try {
        const result = await client.query(queryAccount);
        person_id = result.rows[0].id;
    } catch (e) {
        res.status(500).send(e);
        console.log('e', e);
    }

    const queryPrivateAccount = SQL`INSERT INTO private.account (person_id, email, password_hash) VALUES
    (${person_id}, ${body.email}, 'NOT_PRESENTED')`;

    try {
        const result = await client.query(queryPrivateAccount);
        console.log('success', result);
    } catch (e) {
        res.status(500).send(e);
        console.log('e', e);
    }
    finally {
        client.end();
    }

    res.status(200).send({message: 'Registration completed successfully'});

}
