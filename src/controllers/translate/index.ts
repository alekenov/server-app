import querystring, { ParsedUrlQuery } from 'querystring';
import got from 'got';
import safeEval from 'safe-eval';
import token from 'google-translate-token';
import { getCode, isSupported } from './languages';
import { Request, Response } from 'express';
import url from 'url';

export let Translate = (req: Request, res: Response) =>
{
    const reqUrl               = url.parse(req.url, true);
    const data: ParsedUrlQuery = reqUrl.query;

    res.setHeader('Access-Control-Allow-Origin', '*');

    // Request methods you wish to allow
    res.setHeader('Access-Control-Allow-Methods', 'GET, POST, OPTIONS, PUT, PATCH, DELETE');

    // Request headers you wish to allow
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With,content-type');

    res.setHeader('Content-Type', 'application/json');

    if (!data['text'] || !data['from'] || !data['to'])
    {
        return return404(res);
    }

    translate(data['text'], {from: data['from'], to: data['to']}).then((result: any) =>
    {
        res.statusCode = 200;
        res.send(JSON.stringify({ data: result['text'] }));
        res.end();

    }).catch((err: any) =>
    {
        console.error(err);
        return return404(res);
    });
};

function return404(res: Response): void
{
    res.statusCode = 200;
    res.setHeader('Cache-Control', 'public, max-age=31556952000');
    res.setHeader('Expires', new Date(Date.now() + 31556952000).toUTCString());
    res.setHeader('Content-Type', 'image/png');
    res.end('error');
}

export function translate(text: any, opts: any)
{
    opts = opts || {};

    let e: any;
    [opts.from, opts.to].forEach(function (lang)
    {
        if (lang && !isSupported(lang))
        {
            e         = new Error();
            e.code    = 400;
            e.message = 'The language \'' + lang + '\' is not supported';
        }
    });
    if (e)
    {
        return new Promise(function (resolve, reject)
        {
            reject(e);
        });
    }

    opts.from = opts.from || 'auto';
    opts.to   = opts.to || 'en';

    opts.from = getCode(opts.from);
    opts.to   = getCode(opts.to);

    return token.get(text).then((token2: any) =>
    {
        const url         = 'https://translate.google.com/translate_a/single';
        const data: any   = {
            client: 't',
            sl    : opts.from,
            tl    : opts.to,
            hl    : opts.to,
            dt    : ['at', 'bd', 'ex', 'ld', 'md', 'qca', 'rw', 'rm', 'ss', 't'],
            ie    : 'UTF-8',
            oe    : 'UTF-8',
            otf   : 1,
            ssel  : 0,
            tsel  : 0,
            kc    : 7,
            q     : text
        };
        data[token2.name] = token2.value;

        return url + '?' + querystring.stringify(data);
    }).then((url: string) =>
    {
        return got(url).then((res: any) =>
        {
            const result = {
                text: '',
                from: {
                    language: {
                        didYouMean: false,
                        iso       : ''
                    },
                    text    : {
                        autoCorrected: false,
                        value        : '',
                        didYouMean   : false
                    }
                },
                raw : ''
            };

            if (opts.raw)
            {
                result.raw = res.body;
            }

            const body = safeEval(res.body);
            body[0].forEach((obj: any) =>
            {
                if (obj[0])
                {
                    result.text += obj[0];
                }
            });

            if (body[2] === body[8][0][0])
            {
                result.from.language.iso = body[2];
            }
            else
            {
                result.from.language.didYouMean = true;
                result.from.language.iso        = body[8][0][0];
            }

            if (body[7] && body[7][0])
            {
                let str = body[7][0];

                str = str.replace(/<b><i>/g, '[');
                str = str.replace(/<\/i><\/b>/g, ']');

                result.from.text.value = str;

                if (body[7][5] === true)
                {
                    result.from.text.autoCorrected = true;
                }
                else
                {
                    result.from.text.didYouMean = true;
                }
            }

            return result;
        }).catch((err: any) =>
        {
            let er: any;
            er = new Error();
            if (err.statusCode !== undefined && err.statusCode !== 200)
            {
                er.code = 'BAD_REQUEST';
            }
            else
            {
                er.code = 'BAD_NETWORK';
            }
            throw er;
        });
    });
}
