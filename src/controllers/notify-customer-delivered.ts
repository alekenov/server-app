import { Order } from '../models/order';
import { readFileSync } from 'fs';
import { join } from 'path';
import nodemailer from 'nodemailer';
import handlebars from 'handlebars';
import { MailSmtpSettings } from '../config/mail-smtp-settings';
import { LOGGER } from '../config/logger';

export class NotifyCustomerDelivered
{
  constructor()
  {
  }

  run(data: any): void
  {
    const order = new Order(data.row);

    this.sendEmail(order)
      .then(() =>
      {
        LOGGER.info(`Send delivered email: order id = ${order.id}`);
      })
      .catch(error =>
      {
        LOGGER.error(`Notify delivered, email: ${order.senderEmail}, ` + error.message);
      });
  }

  /**
   * Send email to customer
   * @param {Order} order
   * @returns {Promise<any>}
   */
  private sendEmail(order: Order): Promise<any>
  {
    return new Promise((resolve, reject) =>
    {
      const mailPath    = join('dist', 'public', 'mail-templates', 'order-delivered.html');
      const html        = readFileSync(mailPath).toString();
      const template    = handlebars.compile(html);
      const htmlToSend  = template(order);
      const transporter = nodemailer.createTransport(new MailSmtpSettings().data);

      const mailOptions = {
        to     : order.senderEmail,
        from   : '"admin" <love@cvety.kz>',
        subject: 'Cvety.kz заказ ' + order.orderid,
        html   : htmlToSend
      };
      transporter.sendMail(mailOptions, (error) =>
      {
        if (error)
        {
          reject(error);
        }
        else
        {
          resolve();
        }
      });
    });
  }
}