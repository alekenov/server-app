import request, { Request } from 'request';
import { DbLogger } from './db-logger';

export class Mobizon
{
  public static url = 'https://api.mobizon.com/service/message/sendsmsmessage';
  public static key = 'b00ba10af38ed8a83f5aa157f9c672cd307aa0a7';

  private url: string;
  private key: string;
  private recipient: string;
  public text: string;
  private from: string;

  constructor(data: any)
  {
    this.config();

    data = data || {};

    this.recipient = data.recipient; // '+380960829493';
    this.from      = data.from; // 'сvety';
    this.text      = data.text; // 'Ваш заказ принят';
  }

  private config(): void
  {
    this.url = Mobizon.url;
    this.key = Mobizon.key;
  }

  sendMessage(): Request
  {
    const logger = new DbLogger();

    const req = request.post({
      uri : this.url,
      form: {
        recipient: this.recipient,
        text     : this.text,
        apiKey   : this.key,
        output   : 'json',
        // from     : this.from
      }
    });

    req.on('error', (error) => {
      logger.log(error, 'error');
    });
    req.on('data', (data) => {
      logger.log('Send sms with Mobizon success', data.toString());
    });

    return req;
  }
}
