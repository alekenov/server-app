import { PgMarketplaceListener } from '../apps/marketplace-db-notify-server';
import { Pg } from '../config/pg-client-settings';
import { SQL } from '../utils/sql';
import { Client } from 'pg';
import pg from 'pg';
import { Transliterate } from '../utils/transliterate';
import { translate } from './translate';
import async from 'async';
import { CallbackInterface } from '../utils/callback-interface';
import { LOGGER } from '../config/logger';

export class LekenTranslator
{
  constructor()
  {
    this.watchChanges();
    this.checkTranslates();
  }

  checkTranslates(): void
  {
    const client = new Pg().clientMarket;
    const query  = SQL`select * from base.bouquet b where NOT EXISTS(select id from base.bouquet_translate bt where bt.bouquet_id = b.id)`;

    client.query(query)
      .then((res) =>
      {
        async.each(res.rows, (bouquet: any, callback: CallbackInterface) =>
        {
          this.getTranslates(bouquet).then(() =>
          {
            callback();
          });
        }, (err) =>
        {
        });

        client.end();
      })
      .catch(e =>
      {
        console.log('e', e);
        client.end();
      });
  }

  watchChanges(): void
  {
    PgMarketplaceListener.on('marketplace-listen', (value: any) =>
    {
      if (value.table === 'bouquet')
      {
        const bouquet = value.row;
        this.getTranslates(bouquet);
      }
    });
  }

  /**
   * Get translates
   *
   * @param bouquet
   */
  getTranslates(bouquet: any): Promise<any>
  {
    return new Promise((resolve, reject) =>
    {
      const client: Client        = new Pg().clientMarket;
      const query: pg.QueryConfig = SQL`SELECT *
            FROM base.bouquet_translate bt
            WHERE bt.bouquet_id = ${bouquet.id}`;

      client.query(query)
        .then((res) =>
        {
          this.updateTranslates(res.rows, bouquet);
          client.end();
          resolve();
        })
        .catch(e =>
        {
          console.log('e', e);
          client.end();
          reject();
        });
    });
  }

  /**
   * Update translate
   *
   * @param rows
   * @param bouquet
   */
  updateTranslates(rows: any[], bouquet: any): void
  {
    let translates: any[] = [
      {
        id         : null,
        language_id: 1,
        name       : bouquet.name,
        description: bouquet.description,
        subtitle   : bouquet.subtitle,
        bouquet_id : bouquet.id,
        url        : Transliterate.transliterate(bouquet.name || '')
      },
      {
        id         : null,
        language_id: 2,
        name       : null,
        description: null,
        subtitle   : null,
        bouquet_id : bouquet.id,
        url        : null
      }
    ];

    translates = translates.map(translate =>
    {
      const row = rows.find(r => r.language_id === translate.language_id);

      if (row !== undefined)
      {
        translate.id = row.id;
      }
      return translate;
    });

    Promise.all([
      this.translate(bouquet.name),
      this.translate(bouquet.description),
      this.translate(bouquet.subtitle)
    ])
      .then((value) =>
      {
        translates[1].name        = value[0];
        translates[1].description = value[1];
        translates[1].subtitle    = value[2];
        translates[1].url         = Transliterate.transliterate(value[0]);

        this.saveTranslates(translates);
      });
  }

  /**
   * Save translates
   *
   * @param translates
   */
  saveTranslates(translates: any[]): void
  {
    const client = new Pg().clientMarket;

    async.each(translates, (translate: any, callback: CallbackInterface) =>
    {
      let query;

      if (translate.id)
      {
        query = SQL`UPDATE base.bouquet_translate
                        SET url = ${translate.url},
                        name = ${translate.name},
                        description = ${translate.description},
                        subtitle = ${translate.subtitle}
                        WHERE id = ${translate.id}`;
      }
      else
      {
        query = SQL`INSERT INTO base.bouquet_translate
                        (url, name, description, bouquet_id, subtitle)
                         VALUES (${translate.url}, ${translate.name}, ${translate.description}, ${translate.bouquet_id}, ${translate.subtitle})`;
      }

      client.query(query)
        .then((res) =>
        {
          console.log('success', res);
          callback();
        })
        .catch(e =>
        {
          console.log('e', e);
          callback();
        });
    }, (err) =>
    {
      if (err)
      {
        LOGGER.error('Order moved to abandoned unsuccessfully');
      }
      client.end();
    });
  }

  /**
   * Translate
   *
   * @param text
   */
  translate(text: string): Promise<string>
  {
    text = text || '';
    return new Promise((resolve, reject) =>
    {
      if (text.length === 0)
      {
        resolve('');
        return;
      }

      translate(text, {from: 'ru', to: 'en'}).then((result: any) =>
      {
        resolve(result['text']);

      }).catch((err: any) =>
      {
        resolve('');
        console.error(err);
      });
    });
  }

  updateBouquetUrl(bouquet: any): void
  {
    const url = Transliterate.transliterate(bouquet.name);

    const query  = SQL`UPDATE base.bouquet SET url = ${url}  WHERE id = ${bouquet.id}`;
    const client = new Pg().clientMarket;

    client.query(query)
      .then((res) =>
      {
        console.log('success', res);
        client.end();
      })
      .catch(e =>
      {
        console.log('e', e);
        client.end();
      });
  }
}