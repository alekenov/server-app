import { ProductFromOrder } from './product-from-order';
import moment from 'moment';
import { ToCamelCase } from '../utils/to-camel-case';
import { CamelCaseToDash } from '../utils/camel-case-to-dash';

export class Order
{
  id: number;
  organizationId: number;
  statusId: number;
  ownerId: number;
  amount: number;
  currencyCode: string;
  deliveryDate: string;
  deliveryAddress: string;
  recipientName: string;
  recipientEmail: string;
  recipientPhone: string;
  senderName: string;
  senderPhone: string;
  senderEmail: string;
  postcardText: string;
  createdAt: string;
  city: string;
  pay: string;
  paymentResult: boolean;
  paymentChecked: boolean;
  paymentTime: string;
  orderid: string;
  motive: string;
  productsImages: string[];
  resultImages: string[];
  pickUpYourself: boolean;
  productsdata: string;
  productFromStockIds: number[];
  spent: number;
  balance: number;
  deliveryType: string;
  isNotify: boolean;

  constructor(data: any)
  {

    const order = new ToCamelCase().forObject(data);

    this.id                  = order.id || undefined;
    this.organizationId      = order.organizationId || undefined;
    this.statusId            = order.statusId || undefined;
    this.ownerId             = order.ownerId || undefined;
    this.amount              = order.amount || 0;
    this.currencyCode        = order.currencyCode || undefined;
    this.deliveryDate        = order.deliveryDate || undefined;
    this.deliveryAddress     = order.deliveryAddress || '';
    this.recipientName       = order.recipientName || '-';
    this.recipientEmail      = order.recipientEmail || '-';
    this.recipientPhone      = order.recipientPhone || '-';
    this.senderName          = order.senderName || '-';
    this.senderPhone         = order.senderPhone || '-';
    this.senderEmail         = order.senderEmail || '-';
    this.postcardText        = order.postcardText || '-';
    this.createdAt           = order.createdAt || undefined;
    this.city                = order.city || '';
    this.pay                 = order.pay || '';
    this.paymentResult       = order.paymentResult || false;
    this.paymentChecked      = order.paymentChecked || undefined;
    this.paymentTime         = order.paymentTime || undefined;
    this.orderid             = order.orderid || '';
    this.motive              = order.motive || '';
    this.productsImages      = order.productsImages || [];
    this.resultImages        = order.resultImages || [];
    this.pickUpYourself      = order.pickUpYourself || undefined;
    this.productsdata        = order.productsdata || '[]';
    this.productFromStockIds = order.productFromStockIds || [];
    this.spent               = order.spent || undefined;
    this.balance             = order.balance || undefined;
    this.deliveryType        = order.deliveryType || '';
    this.isNotify            = order.isNotify || false;
  }

  get dataToDash(): any
  {
    return new CamelCaseToDash().forObject(this);
  }

  get mainImage(): string
  {
    return this.productsImages.length > 0
      ? this.productsImages[0]
      : 'https://crm.cvety.kz/assets/images/logos/flow_1.svg';
  }

  get products(): ProductFromOrder[]
  {
    const products = JSON.parse(this.productsdata);

    if (Array.isArray(products))
    {
      return products.map(item => new ProductFromOrder(item));
    }
    else
    {
      return [];
    }
  }

  get productName(): string
  {
    if (this.products.length > 0)
    {
      return this.products
        .map(product => product.name)
        .join(', ');

    }
    else
    {
      return '';
    }
  }

  currencyLabel(): string
  {
    if (this.currencyCode.toUpperCase() === 'KZT')
    {
      return 'тг';
    }
    else if (this.currencyCode.toUpperCase() === 'USD')
    {
      return '$';
    }
    else
    {
      return '';
    }
  }

  paymentStatusLabel(): string
  {
    return this.paymentResult ? 'Оплачено' : 'Не оплачено';
  }

  get deliveryDateFormat(): string
  {
    return this.deliveryDate ? moment(this.deliveryDate).format('DD.MM.YYYY') : '';
  }
}
