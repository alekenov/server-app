export class ProductFromOrder {
  id: number;
  isMore_30: boolean;
  isMore_60: boolean;
  price: number;
  name: string;
  img: string;

  constructor(data: any)
  {
    this.id = data.id || undefined;
    this.isMore_30 = data.isMore_30 || false;
    this.isMore_60 = data.isMore_60 || false;
    this.price = data.price || 0;
    this.name = data.name || '';
    this.img = data.img || 'https://crm.cvety.kz/assets/images/logos/flow_1.svg';
  }
}
