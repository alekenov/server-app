import { SocketServer } from './apps/socket-server';
import { ResizeServer } from './apps/resize-server';
import { StockDbNotifyServer } from './apps/stock-db-notify-server';
import { ScheduleServer } from './apps/schedule-server';
import { StockDbServerGQL } from './apps/postgraphile-stock-db-server';
import { MarketplaceDbNotifyServer } from './apps/marketplace-db-notify-server';
import { StockDbAnonServerGQL } from './apps/postgraphile-stock-db-anon-server';
import { TranslateServer } from './apps/translate-server';
import { BalloonDbServerGQL } from './apps/postgraphile-balloon-db-server';
import { BalloonDbAnonServerGQL } from './apps/postgraphile-balloon-db-anon-server';
import { LekenTranslator } from './controllers/leken-translator';
import { CvetyTranslator } from './controllers/cvety-translator';
import { PostgraphileStockDbServer2 } from './apps/postgraphile-stock-db-server2';
import { LekenApi } from './apps/leken-api';
import { NotifySender } from './controllers/notify-sender';
import { CvetyDbServerGQL } from './apps/postgraphile-cvety-db-server';

export let ENV_TYPE = 'prod'; // dev or prod

/**
 * Run processes every 1 minute
 */
const ScheduleApp = new ScheduleServer().getApp();
export { ScheduleApp };

/**
 * WebSocket server on stock db.
 */
const SocketApp = new SocketServer().getApp();
export { SocketApp };

/**
 * Resize images server.
 */
const ResizeApp = new ResizeServer().getApp();
export { ResizeApp };

/**
 * Stock db triggers listen.
 */
const StockListen = new StockDbNotifyServer().getApp();
export { StockListen };

/**
 * Marketplace db triggers listen.
 */
const MarketplaceListen = new MarketplaceDbNotifyServer().getApp();
export { MarketplaceListen };

/**
 * Graphql server for stock database
 */
const StockDbGQL = new StockDbServerGQL().getApp();
export { StockDbGQL };

/**
 * Graphql server for stock database anonymous
 */
const StockDbAnonGQL = new StockDbAnonServerGQL().getApp();
export { StockDbAnonGQL };

const stockDb2 = new PostgraphileStockDbServer2().getApp();
export { stockDb2 };

/**
 * Graphql server for stock database
 */
const BalloonDbGQL = new BalloonDbServerGQL().getApp();
export { BalloonDbGQL };

/**
 * Graphql server for stock database anonymous
 */
const BalloonDbAnonGQL = new BalloonDbAnonServerGQL().getApp();
export { BalloonDbAnonGQL };

/**
 * Graphql server for cvety database anonymous
 */
const cvetyDbServerGQL = new CvetyDbServerGQL().getApp();
export { CvetyDbServerGQL };

/**
 * Translate server.
 */
const translateApp = new TranslateServer().getApp();
export { translateApp };

/**
 * Translate leken bouquets
 */
const translateLeken = new LekenTranslator();
export { translateLeken };

/**
 * Translate cvety bouquets
 */
const translateCvety = new CvetyTranslator();
export { translateCvety };

const lekenApi = new LekenApi();
export { lekenApi };

const notifySender = new NotifySender();