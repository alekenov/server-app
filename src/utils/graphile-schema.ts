import fs from 'fs';

export function getGraphileSchema(fileName: string) {
  // Read schema from the local file
  const rawSchema = JSON.parse(fs.readFileSync(fileName, 'utf8'));

  // Get configs with plural names
  const queryObj = rawSchema.data.__schema.types.find((type: any) => type.name === 'Query');
  const queries  = queryObj.fields.filter((field: any) => field.name.startsWith('all'));

  // Transform schema
  return rawSchema.data.__schema.types
    .filter((type: any) =>
    {
      return type.fields && type.kind === 'OBJECT' && type.fields[0].name === 'nodeId';
    })
    .map((type: any) =>
    {
      const pluralQueryName = queries.find((item: any) => item.description.includes(`\`${type.name}\``)).name;
      const pluralName      = pluralQueryName.substr(3);
      const fields          = getFields(type.fields);

      return {
        name: type.name,
        pluralName,
        fields
      };
    });
}

/**
 * Get entity fields
 *
 * @param fields
 */
function getFields(fields: any[]): any
{
  return fields
    .filter((field: any) =>
    {
      if (field.args && field.args.length > 0)
      {
        return false;
      }
      return !(field.type.ofType && field.type.ofType.name.endsWith('Connection'));
    })
    .reduce((_fields: any, field: any) =>
    {
      let type;

      switch (field.type.kind)
      {
        case 'LIST':
          type = field.type.ofType.name + '[]';
          break;

        case 'OBJECT':
          type = {
            kind: 'OBJECT',
            name: field.type.name
          };
          break;

        default:
          type = !!field.type.ofType ? field.type.ofType.name : field.type.name;
          break;
      }

      return {
        ..._fields,
        [field.name]: type
      };
    }, {});
}