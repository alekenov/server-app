import { createServer, Server } from 'https';
import express from 'express';
import { postgraphile } from 'postgraphile';
import PostGraphileConnectionFilterPlugin from 'postgraphile-plugin-connection-filter';
import { Ssl } from '../config/ssl';
import { Pg } from '../config/pg-client-settings';

export class BalloonDbAnonServerGQL
{
    public static readonly PORT: number = 5214;
    private app: express.Application;
    private server: Server;
    private port: string | number;

    constructor()
    {
        this.createApp();
        this.config();
        this.createServer();
        this.listen();
    }

    private createApp(): void
    {
        this.app = express();

        this.app.use(
            postgraphile(Pg.balloonDbConfigString(), 'base', {
                graphiql     : true,
                appendPlugins: [PostGraphileConnectionFilterPlugin],
                pgDefaultRole: 'base_anonymous',
                enableCors: true,
                jwtSecret: 'keyboard_kitten',
                jwtPgTypeIdentifier: 'base.jwt_token'
            })
        );
    }

    private createServer(): void
    {
        this.server = createServer(Ssl.floraOptions(), this.app);
    }

    private config(): void
    {
        this.port = BalloonDbAnonServerGQL.PORT;
    }

    private listen(): void
    {
        this.server.listen(this.port, () =>
        {
            console.log('Running graphile stock-db server on port %s', this.port);
        });
    }

    public getApp(): express.Application
    {
        return this.app;
    }
}