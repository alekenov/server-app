import { Pg } from '../config/pg-client-settings';
import { LOGGER } from '../config/logger';
import { Esputnik } from '../controllers/esputnik';
import { NotifyCustomerDelivered } from '../controllers/notify-customer-delivered';
import { NotifyCustomerPayed } from '../controllers/notify-customer-payed';
import { EventEmitter } from 'events';
import { Mobizon } from '../controllers/mobizon';
import { DbLogger } from '../controllers/db-logger';

export const PgListener = new EventEmitter();

export class StockDbNotifyServer
{
  public static readonly channels: string[] = [
    'deal_status_updated',
    'deal_payed',
    'table_update'
  ];
  private app: any;

  constructor()
  {
    this.createApp();
    this.connect();
  }

  private createApp(): void
  {
    this.app = new Pg().client;
  }

  private connect(): void
  {
    this.app.connect((err: Error) =>
    {
      if (err)
      {
        LOGGER.error(err.message);
      }
      this.app.on('notification', function (msg: any)
      {
        tell(msg);
      });

      for (const e of StockDbNotifyServer.channels)
      {
        this.app.query('LISTEN ' + e);
      }
    });
  }

  public getApp(): any
  {
    return this.app;
  }
}

/**
 * Notify trigger init
 * @param msg
 * @returns {any}
 */
const tell = (msg: any): any =>
{
  if (msg && msg.hasOwnProperty('channel'))
  {
    if (typeof msg.payload !== 'string' || !msg.payload)
    {
      return undefined;
    }

    const object = JSON.parse(msg.payload) || {row: {}};

    switch (msg.channel)
    {
      case 'deal_status_updated':

        if (object.row.status_id === 9)
        {
          new Esputnik().run(object);
          new NotifyCustomerDelivered().run(object);
        }

        // Sending sms using Mobizon
        sendSms(object);

        break;

      case 'deal_payed':
        new NotifyCustomerPayed().run(object);
        break;

      case 'table_update':
        PgListener.emit('pg-listen', object);
        break;
    }

    // console.log(object);
  }
};

/**
 * Sending sms using Mobizon
 *
 * @param data
 */
function sendSms(data: any): void
{
  switch (data.row.status_id)
  {
    case 4:

      mobizonSend(
        data.row.sender_phone,
        `Vash Zakaz ${data.row.orderid} prinyat. Status Zakaza https://onetwoflowers.ru/os/${data.row.random_uuid}/`
      );

      break;

    case 9:

      mobizonSend(
        data.row.sender_phone,
        `Оставьте отзыв к заказу - https://onetwoflowers.ru/o/${data.row.random_uuid}/`
      );

      mobizonSend(
        data.row.recipient_phone,
        `Оставьте отзыв к заказу - https://onetwoflowers.ru/rf/${data.row.random_uuid}/`
      );

      break;
  }
}

function mobizonSend(phone: string, text: string)
{
  if (!phone)
  {
    return;
  }

  const logger = new DbLogger();
  const client = new Mobizon({
    recipient: phone,
    from     : 'сvety',
    text     : text
  });

  client.sendMessage();
  logger.log(client.text);
}
