import { createServer, Server } from 'https';
import express from 'express';
import { postgraphile } from 'postgraphile';
import GraphileBuildPgContribConnectionFilter from 'postgraphile-plugin-connection-filter';
import { Ssl } from '../config/ssl';
import { Pg } from '../config/pg-client-settings';
import { MemoryCache } from '../utils/cache';
import { Response, Request } from 'express';
import compression from 'compression';
import cors from 'cors';
import { getGraphileSchema } from '../utils/graphile-schema';

const corsOptions = {
  origin              : '*',
  optionsSuccessStatus: 200
};

export class StockDbServerGQL
{
  public static readonly PORT: number = 5209;
  private app: express.Application;
  private server: Server;
  private port: string | number;

  constructor()
  {
    this.createApp();
    this.enableCors();
    this.enableSchemaRoute();
    this.config();
    this.createServer();
    this.listen();
  }

  private createApp(): void
  {
    this.app = express();
    this.app.use(compression());

    this.app.use(
      postgraphile(Pg.stockDbConfigString(), 'base', {
        graphiql            : true,
        appendPlugins       : [GraphileBuildPgContribConnectionFilter],
        pgDefaultRole       : 'base_anonymous',
        enableCors          : true,
        jwtSecret           : 'keyboard_kitten',
        jwtPgTypeIdentifier : 'base.jwt_token',
        exportJsonSchemaPath: 'graphile-schema.json',
        disableQueryLog     : true,
        dynamicJson         : true,
        jwtVerifyOptions    : {
          ignoreExpiration: true,
        }
      })
    );
  }

  private enableSchemaRoute(): void
  {
    this.app.use((req: any, res: any, next: any) =>
    {
      req.connection.setNoDelay(true);
      next();
    });

    this.app.get('/graphile-schema', MemoryCache(), getSchema);
  }

  private enableCors(): void
  {
    this.app.use(cors(corsOptions));
  }

  private createServer(): void
  {
    this.server = createServer(Ssl.floraOptions(), this.app);
  }

  private config(): void
  {
    this.port = StockDbServerGQL.PORT;
  }

  private listen(): void
  {
    this.server.listen(this.port, () =>
    {
      console.log('Running graphile stock-db server on port %s', this.port);
    });
  }

  public getApp(): express.Application
  {
    return this.app;
  }
}

/**
 * Get schema
 *
 * @param req
 * @param res
 */
function getSchema(req: Request, res: Response)
{
  // graphile-schema.json
  const schema = getGraphileSchema('graphile-schema.json');

  return res.send(schema);
}

