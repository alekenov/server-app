import express, { Request, Response, NextFunction } from 'express';
import { createServer, Server } from 'https';
import compression from 'compression';
import bodyParser  from 'body-parser';
import { postgraphile } from 'postgraphile';
import GraphileBuildPgContribConnectionFilter from 'postgraphile-plugin-connection-filter';

import { MemoryCache } from '../utils/cache';
import { Ssl } from '../config/ssl';

import { makeExtendSchemaPlugin, gql } from 'graphile-utils';
import { sendCode, chargeCard, registerShop } from '../controllers/leken';
import { getGraphileSchema } from '../utils/graphile-schema';

const SCHEMA_NAME = 'leken-schema.json';

const MyForeignExchangePlugin = makeExtendSchemaPlugin(build => {
  const { pgSql: sql } = build;
  return {
    typeDefs: gql`
      extend type Bouquet {
        priceFloat: Float! @requires(columns: ["price"])
      }
    `,
    resolvers: {
      Bouquet: {
        priceFloat: async (
          bouquet
        ) => {
          const { price } = bouquet;
          return await parseFloat(price);
        },
      },
    },
  };
});

export class LekenApi
{
  public static readonly PORT: number = 5201;
  private app: express.Application;
  private server: Server;
  private port: string | number;

  constructor()
  {
    this.createApp();
    this.config();
    this.createServer();
    this.listen();
  }

  private createApp(): void
  {
    this.app = express();
    this.app.use(compression());
    this.app.use(bodyParser.json());
    this.app.use(bodyParser.urlencoded());


      this.app.use(
      postgraphile('postgres://postgres:aOsQRYw8agiD@localhost:5432/marketplace', 'base', {
        graphiql           : true,
        // appendPlugins      : [GraphileBuildPgContribConnectionFilter, MyForeignExchangePlugin],
        appendPlugins      : [GraphileBuildPgContribConnectionFilter],
        pgDefaultRole      : 'base_person', // base_anonymous
        enableCors         : true,
        jwtSecret          : 'keyboard_kitten',
        jwtPgTypeIdentifier: 'base.jwt_token',
        exportJsonSchemaPath: SCHEMA_NAME,
        disableQueryLog    : true,
        dynamicJson        : true,
        jwtVerifyOptions   : {
          ignoreExpiration: true,
        }
      })
    );

    this.app.post('/payments/cards/charge', chargeCard);
    this.app.post('/register-shop', registerShop);
    this.app.get('/send-code/:phone', sendCode);
    this.app.get('/graphile-schema', MemoryCache(), getSchema);
  }

  private createServer(): void
  {
    this.server = createServer(Ssl.floraOptions(), this.app);
  }

  private config(): void
  {
    this.port = LekenApi.PORT;
  }

  private listen(): void
  {
    this.server.listen(this.port, () =>
    {
      console.log('Running graphile stock-db server on port %s', this.port);
    });
  }

  public getApp(): express.Application
  {
    return this.app;
  }
}

/**
 * Get schema
 *
 * @param req
 * @param res
 */
function getSchema(req: Request, res: Response)
{
  // graphile-schema.json
  const schema = getGraphileSchema(SCHEMA_NAME);

  return res.send(schema);
}
