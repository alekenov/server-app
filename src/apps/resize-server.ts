import { createServer, Server } from 'https';
import express from 'express';
import { ResizeImages, ResizeImageStatic } from '../controllers/resize-images';
import { Ssl } from '../config/ssl';

export class ResizeServer
{
  public static readonly PORT: number = 1338;
  private app: express.Application;
  private server: Server;
  private port: string | number;

  constructor()
  {
    this.createApp();
    this.config();
    this.imageServerStatic();
    this.createServer();
    this.listen();
  }

  private createApp(): void
  {
    this.app = express();
  }

  private createServer(): void
  {
    this.server = createServer(Ssl.floraOptions(), this.app);
  }

  private config(): void
  {
    this.port = ResizeServer.PORT;

    this.app.use(ResizeImages);
  }

  private imageServerStatic(): void
  {
    // this.app.use('/cvety', express.static('/opt/cvety/images'));
    this.app.use('/cvety', ResizeImageStatic);
  }

  private listen(): void
  {
    this.server.listen(this.port, () =>
    {
      console.log('Running resize images server on port %s', this.port);
    });
  }

  public getApp(): express.Application
  {
    return this.app;
  }
}