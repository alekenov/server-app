import { Client } from 'pg';
import { Pg } from '../config/pg-client-settings';
import { LOGGER } from '../config/logger';
import { EventEmitter } from 'events';
import { Mobizon } from '../controllers/mobizon';

export const PgMarketplaceListener = new EventEmitter();

export class MarketplaceDbNotifyServer
{
  public static readonly channels: string[] = [
    'deal_status_updated',
    'table_update',
  ];
  private app: any;

  constructor()
  {
    this.createApp();
    this.connect();
  }

  private createApp(): void
  {
    this.app = new Client(Pg.marketplaceDbConfigObject());
  }

  private connect(): void
  {
    this.app.connect((err: Error) =>
    {
      if (err)
      {
        LOGGER.error(err.message);
      }
      this.app.on('notification', function (msg: any)
      {
        tell(msg);
      });

      for (const e of MarketplaceDbNotifyServer.channels)
      {
        this.app.query('LISTEN ' + e);
      }
    });
  }

  public getApp(): any
  {
    return this.app;
  }
}

/**
 * Notify trigger init
 * @param msg
 * @returns {any}
 */
const tell = (msg: any): any =>
{
  if (msg && msg.hasOwnProperty('channel'))
  {
    if (typeof msg.payload !== 'string' || !msg.payload)
    {
      return undefined;
    }

    const object = JSON.parse(msg.payload) || {current_row: {}, old_row: {}, table: '', type: ''};

    switch (msg.channel)
    {
      case 'table_update':
        PgMarketplaceListener.emit('marketplace-listen', object);
        break;

      case 'deal_status_updated':
        // Sending sms using Mobizon
        mobizonSend(object);
        break;
    }

    // console.log(object);
  }
};


/**
 * Sending sms using Mobizon
 *
 * @param data
 */
function mobizonSend(data: any): void
{
  if (data.row.status_id === 4 && !!data.row.sender_phone)
  {
    const moby = new Mobizon({
      recipient: data.row.sender_phone,
      from     : 'сvety',
      text     : `Vash Zakaz ${data.row.id} prinyat. Status Zakaza https://lkn.kz/${data.row.random_uuid}`
    });

    moby.sendMessage();
  }
}