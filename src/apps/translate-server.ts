import { createServer, Server } from 'https';
import express from 'express';
import { Translate } from '../controllers/translate';
import { Ssl } from '../config/ssl';

export class TranslateServer
{
    public static readonly PORT: number = 5313;
    private app: express.Application;
    private server: Server;
    private port: string | number;

    constructor()
    {
        this.createApp();
        this.config();
        this.createServer();
        this.listen();
    }

    private createApp(): void
    {
        this.app = express();
    }

    private createServer(): void
    {
        this.server = createServer(Ssl.floraOptions(), this.app);
    }

    private config(): void
    {
        this.port = TranslateServer.PORT;

        this.app.use(Translate);
    }

    private listen(): void
    {
        this.server.listen(this.port, () =>
        {
            console.log('Running translate server on port %s', this.port);
        });
    }

    public getApp(): express.Application
    {
        return this.app;
    }
}