import * as schedule from 'node-schedule';
import { ENV_TYPE } from '../server';
import { NotifyCustomerNotPayed } from '../controllers/notify-customer-not-payed';
import { MoveOrderToAbandoned } from '../controllers/move-order-to-abandoned';
import { LekenPaymentCheck } from '../controllers/leken-payment-check';

export class ScheduleServer
{
  static controllers(): any
  {
    if (ENV_TYPE === 'prod')
    {
      return () =>
      {
        new NotifyCustomerNotPayed().run();
        new MoveOrderToAbandoned().run();
        new LekenPaymentCheck().run();
      };
    }
    else
    {
      return () =>
      {
        new LekenPaymentCheck().runTest();
      };
    }
  }

  private app: any;

  constructor()
  {
    this.createApp();
    this.scheduleJob();
  }

  private createApp(): void
  {
    this.app = schedule;
  }

  private scheduleJob(): void
  {
    this.app.scheduleJob('*/1 * * * *', ScheduleServer.controllers());
    // new LekenPaymentCheck().runTest();
  }

  public getApp(): any
  {
    return this.app;
  }
}