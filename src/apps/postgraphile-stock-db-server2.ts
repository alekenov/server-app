import express from 'express';
import { createServer, Server } from 'https';
import compression from 'compression';
import { postgraphile } from 'postgraphile';
import { Pg } from '../config/pg-client-settings';
import GraphileBuildPgContribConnectionFilter from 'postgraphile-plugin-connection-filter';
import { Ssl } from '../config/ssl';

export class PostgraphileStockDbServer2
{
  public static readonly PORT: number = 5218;
  private app: express.Application;
  private server: Server;
  private port: string | number;

  constructor()
  {
    this.createApp();
    this.config();
    this.createServer();
    this.listen();
  }

  private createApp(): void
  {
    this.app = express();
    this.app.use(compression());

    this.app.use(
      postgraphile(Pg.stockDbConfigString(), 'base', {
        graphiql            : true,
        appendPlugins       : [GraphileBuildPgContribConnectionFilter],
        pgDefaultRole       : 'base_anonymous',
        enableCors          : true,
        jwtSecret           : 'keyboard_kitten',
        jwtPgTypeIdentifier : 'base.jwt_token',
        exportJsonSchemaPath: 'graphile-schema.json',
        disableQueryLog     : true,
        dynamicJson         : false,
        jwtVerifyOptions    : {
          ignoreExpiration: true,
        }
      })
    );
  }

  private createServer(): void
  {
    this.server = createServer(Ssl.floraOptions(), this.app);
  }

  private config(): void
  {
    this.port = PostgraphileStockDbServer2.PORT;
  }

  private listen(): void
  {
    this.server.listen(this.port, () =>
    {
      console.log('Running graphile stock-db server on port %s', this.port);
    });
  }

  public getApp(): express.Application
  {
    return this.app;
  }
}
