import { Client } from 'pg';
import { ENV_TYPE } from '../server';

export class Pg
{
  public static balloonDbConfigString(): string
  {
    return ENV_TYPE === 'dev'
      ? 'postgres://postgres:Parol@localhost:5432/balloon'
      : 'postgres://postgres:aOsQRYw8agiD@localhost:5432/balloon';
  }

  public static stockDbConfigString(): string
  {
    return ENV_TYPE === 'dev'
      ? 'postgres://postgres:Parol@localhost:5432/stock_1'
      : 'postgres://postgres:aOsQRYw8agiD@localhost:5432/stock';
  }

  public static cvetyDbConfigString(): string
  {
    return 'postgres://postgres:aOsQRYw8agiD@localhost:5432/cvety';
  }

  public static marketplaceDbConfigString(): string
  {
    return ENV_TYPE === 'dev'
      ? 'postgres://postgres:Parol@localhost:5432/marketplace'
      : 'postgres://postgres:aOsQRYw8agiD@localhost:5432/marketplace';
  }

  public static marketplaceDbConfigObject(): any
  {
    return ENV_TYPE === 'dev'
      ? {
        user    : 'postgres',
        host    : 'localhost',
        database: 'marketplace',
        password: 'Parol',
        port    : 5432
      }
      : {
        user    : 'postgres',
        host    : 'localhost',
        database: 'marketplace',
        password: 'aOsQRYw8agiD',
        port    : 5432
      };
  }

  public static stockDbConfigObject(): any
  {
    return ENV_TYPE === 'dev'
      ? {
        user    : 'postgres',
        host    : 'localhost',
        database: 'stock_1',
        password: 'Parol',
        port    : 5432
      }
      : {
        user    : 'postgres',
        host    : 'localhost',
        database: 'stock',
        password: 'aOsQRYw8agiD',
        port    : 5432
      };
  }

  constructor()
  {
  }

  get client(): Client
  {
    const client = new Client(Pg.stockDbConfigObject());
    client.connect();
    return client;
  }

  get clientMarket(): Client
  {
    const client = new Client(Pg.marketplaceDbConfigObject());
    client.connect();
    return client;
  }
}
