import fs from 'fs';

export class Ssl
{
  public static floraOptions(): any
  {
    return {
      key : fs.readFileSync('/etc/letsencrypt/live/floracrm.com/privkey.pem', 'ascii'),
      cert: fs.readFileSync('/etc/letsencrypt/live/floracrm.com/fullchain.pem', 'ascii')
    };
  }
}
