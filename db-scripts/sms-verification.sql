CREATE OR REPLACE FUNCTION private.crypt_code() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  NEW.code := crypt(NEW.code, gen_salt('bf'));
  RETURN NEW;
END;
$$;

CREATE TABLE base.sms_verification (
  phone character varying(255) NOT NULL,
  code text NOT NULL,
  created_at timestamp without time zone DEFAULT now(),
  updated_at timestamp without time zone DEFAULT now()
);

ALTER TABLE ONLY base.sms_verification ADD CONSTRAINT sms_verification_pkey PRIMARY KEY (phone);
CREATE INDEX sms_verification_phone_index ON base.sms_verification (phone);
CREATE TRIGGER sms_verification_updated_at BEFORE INSERT OR UPDATE ON base.sms_verification FOR EACH ROW EXECUTE PROCEDURE private.set_updated_at();
CREATE TRIGGER sms_verification_code  BEFORE INSERT OR UPDATE ON base.sms_verification FOR EACH ROW EXECUTE PROCEDURE private.crypt_code();



CREATE OR REPLACE FUNCTION base.authenticate_phone_code(phone text, code text) RETURNS base.jwt_token
    LANGUAGE plpgsql STRICT SECURITY DEFINER
    AS $_$
DECLARE
  account private.account;
  ver base.sms_verification;
BEGIN
  SELECT v.*
  INTO ver
  FROM base."sms_verification" as v
  WHERE v.phone = $1;

  IF ver.updated_at < NOW() - INTERVAL '1 hour'
  THEN
    RAISE EXCEPTION 'CODE_HAS_BEEN_EXPIRED';
  END IF;

  IF ver.code != crypt($2, ver.code)
  THEN
    RAISE EXCEPTION 'CODE_MISMATCHED';
  END IF;

  SELECT a.*
  INTO account
  FROM private.account AS a
  INNER JOIN base."user" AS u ON u.id = a.person_id
  WHERE u.phone_mob = $1 OR u.phone_work = $1;

  IF account.person_id NOTNULL
  THEN
    RETURN ('base_postgraphql', account.person_id) :: base.jwt_token;
  ELSE
    RETURN NULL;
  END IF;
END;
$_$;

CREATE OR REPLACE FUNCTION base.register_shop(shop text, phone_mob text, email text, city_codes text[], address text, address_translate text, schedule json) RETURNS base."user"
    LANGUAGE plpgsql
    AS $_$
DECLARE
  person       base."user";
  organization base.organization;
BEGIN

  INSERT INTO base.organization (name, phone, city_codes, address, address_translate, schedule, enabled)
  VALUES (shop, phone_mob, city_codes, address, address_translate, schedule, TRUE )
  RETURNING *
    INTO organization;

  INSERT INTO base.user (first_name, phone_mob, email, organization_id)
  VALUES
    (shop, phone_mob, email, organization.id)
  RETURNING *
    INTO person;

  INSERT INTO private.account (person_id, email, password_hash) VALUES
    (person.id, email, 'NOT_PRESENTED');

  RETURN person;
END;
$_$;

ALTER TABLE base.organization ADD COLUMN schedule JSON; 
COMMENT ON COLUMN base.organization.schedule IS 'Режим работы в JSON формате. Пример {
            	"mo": {
            		"start": "11:00",
            		"end": "17:00"
            	},
            	"tu": false,
            	"we": {
            		"start": "11:00",
            		"end": "17:00"
            	},
            	"th": false,
            	"fr": false,
            	"sa": false,
            	"su": false
            }';